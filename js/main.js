$(document).ready(function($) {
    $('*[data-dropdown]').click(function(ev) {
        // var selector = $("#menu-login > div");
        // selector.toggleClass('active');
        var container = $(this);
        var dropdowns = container.parent().find('.dropdown');
        var target = $(ev.target);
        if(!target.is('.dropdown') && !target.parents().is('.dropdown')) {
            var dropdown = container.find('.dropdown');
            if(dropdown.is(':visible')) {
                dropdown.hide();
                container.removeClass('active');
            } else {
                dropdowns.hide();
                container.parent().find('.active').removeClass('active')
                container.addClass('active');
                dropdown.show();
            }
        }
    });

    $(this).click(function(ev) {
        var target = ev.target;
        if (!$(target).is('.dropdown')
            && !$(target).is('.fa.fa-key')
            && !$(target).is('*[data-dropdown]')
            && !$(target).parents().is('*[data-dropdown]')
        )
            {
                $("#menu-login > div").removeClass('active');
                $(".dropdown:visible").hide();
        }
    })
});
